//
//  ContentView.swift
//  Animals
//
//  Created by Sander de Groot on 28/08/2020.
//  Copyright © 2020 Sander de Groot. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    let animals = ["🐈","🐆","🦌","🦒","🦏","🐄","🐀","🦩","🦜"]
    @State var sliderValue: CGFloat = 1
    
    var body: some View {
        NavigationView {
            
            VStack {
                
                Slider(value: $sliderValue, in: 1...CGFloat(animals.count))
                
                Text("\(Int(sliderValue))")
                    .font(.system(size: 20))
                    .fontWeight(.bold)
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .clipShape(Circle())
                
                List(self.animals.chunked(size: Int(sliderValue)), id: \.self) { chunks in
                    ForEach(chunks, id: \.self) { animal in
                        Text(animal)
                            .font(.system(size: 280 / self.sliderValue))
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
