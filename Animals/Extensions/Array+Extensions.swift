//
//  Array+Extensions.swift
//  Animals
//
//  Created by Sander de Groot on 28/08/2020.
//  Copyright © 2020 Sander de Groot. All rights reserved.
//

import Foundation

extension Array {
    
    func chunked(size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0..<Swift.min($0 + size, count)])
        }
    }
}
